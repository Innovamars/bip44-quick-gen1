#!/bin/bash
importmnemonic="$(./efin-cli -rpcuser=user -rpcpassword=password -rpcport=8332 extkeygenesisimport "hard found deputy victory road crop guitar vessel seminar rib december fog reunion leader shaft joy earth start boss three cake basic advice awkward" '')"
echo importmnemonic
ADDRESSES=""
PRIVKEYS=""
for i in 0 1 2 3 4 5 6 7 8 9
do
  NEWADDRESS="$(./efin-cli -rpcuser=user -rpcpassword=password -rpcport=8332 getnewaddress)"
  ADDRESSES="${ADDRESSES} ${NEWADDRESS}"
  NEWPRIVKEY="$(./efin-cli -rpcuser=user -rpcpassword=password -rpcport=8332 dumpprivkey "$NEWADDRESS")"
  PRIVKEYS="${PRIVKEYS} ${NEWPRIVKEY}"
done

for ADDRESS in ${ADDRESSES}; do
    echo ${ADDRESS};
done

for PRIVKEY in ${PRIVKEYS}; do
    echo ${PRIVKEY};
done